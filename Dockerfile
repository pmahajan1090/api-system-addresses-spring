FROM openjdk:17-alpine

# Refer to Maven build -> finalName
ARG JAR_FILE=target/spring-boot-web.jar

WORKDIR /opt/app

COPY ./target/*.jar app.jar

# EXPOSED Application Port
EXPOSE 20102

ENTRYPOINT ["java","-jar","app.jar"]