package net.bglgroup.system.addresses.service;


import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.bglgroup.spring.common.apiframework.requests.ApiRequest;
import net.bglgroup.spring.common.rpgcallprogram.errors.RPGException;
import net.bglgroup.system.addresses.model.Address;
import net.bglgroup.system.addresses.repository.LDI6030;
import net.bglgroup.system.addresses.repository.LDI6038;
import net.bglgroup.system.addresses.repository.TBI6020;

@Service
public class AddressesService 
{					
	@Autowired 
	private LDI6030 ldi6030;
	
	@Autowired 
	private LDI6038 ldi6038;
	
	@Autowired 
	private TBI6020 tbi6020;
	
	public Address getAddressById(String addressId, ApiRequest request) throws RPGException
	{          
          return (Address) ldi6030.callProgram(addressId, request);      
    }

	public ArrayList<Address> getAddressByPostcode(ApiRequest request) throws RPGException
	{          		
        return (ArrayList<Address>) tbi6020.callProgram(request);      
    }
	
	public void patchAddressById(String addressId, ApiRequest request) throws RPGException
	{          
         ldi6038.callProgram(addressId, request);   
    }

}
