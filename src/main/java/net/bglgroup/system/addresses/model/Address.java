package net.bglgroup.system.addresses.model;

import java.util.List;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import net.bglgroup.spring.common.apiframework.model.Action;


@ToString
@NoArgsConstructor
@Getter
@Setter
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"id", "line1", "line2", "line3", "line4", "line5", "postcode", "actions" })
@Component
public class Address
{
    private String id;
	private String line1;
    private String line2;
    private String line3;
    private String line4;
    private String line5;
    private String postcode;
    
    private List<Action> actions;
    
    
    public Address(String id, String line1, String line2, String line3, String line4, String line5, String postcode, List<Action> actions)
    {
    	this.setId(id);
    	this.setLine1(line1);
        this.setLine2(line2);
        this.setLine3(line3);
        this.setLine4(line4);
        this.setLine5(line5);
        this.setPostcode(postcode);
        this.setActions(actions);
    }	
}
