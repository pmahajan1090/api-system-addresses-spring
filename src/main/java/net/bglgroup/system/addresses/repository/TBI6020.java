package net.bglgroup.system.addresses.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import net.bglgroup.spring.common.rpgcallprogram.errors.RPGException;
import net.bglgroup.spring.common.rpgcallprogram.requests.RPGRequest;
import net.bglgroup.spring.common.rpgframework.configuration.ConnectionDetailsConfiguration;
import net.bglgroup.spring.common.rpgframework.programcall.RPGProgram;
import net.bglgroup.spring.common.apiframework.requests.ApiRequest;
import net.bglgroup.spring.common.apientry.headers.Headers;
import net.bglgroup.system.addresses.model.Address;

@Component
public class TBI6020
{	
	private final String programName = "TBI6020";
			
	
	@Autowired
	private Headers apiHeaders;
	
	@Autowired
	private RPGProgram rpgProgram;
	
	@Autowired
	private ConnectionDetailsConfiguration connectionDetailsProperties;
	
	private RPGRequest createRPGRequest(ApiRequest request)
	{		
		return new RPGRequest(connectionDetailsProperties.getReadOnlyConnectionDetails(request.isBasketContext()), 
				              programName, apiHeaders.transformRequestHeaders(request.getHeadersMap()), transformRequest(request));
    }
	
	public ArrayList<Address> callProgram(ApiRequest request) throws RPGException
	{   		
        RPGRequest rpgRequest = createRPGRequest(request);
		
		//rpgProgram.callProgram(rpgRequest);	
		
		return tranformResponse();
		
	}
	
	private Map<String, Object> transformRequest(ApiRequest request)
	{				
        var inputBodyHashMap = new HashMap<String,Object>();
			
		inputBodyHashMap.put("POSTCODE",		     request.getQueryParemeter("postcode"));
		inputBodyHashMap.put("PROPERTYNUMBERORNAME", request.getQueryParemeter("propertynumberorname"));
		
		
		return inputBodyHashMap;
	}
	
	protected ArrayList<Address> tranformResponse() throws RPGException
	{		
		var addressesList = new ArrayList<Address>();
		
		int addressCount = 180;	
		
		for (int i=1; i < addressCount; i++)
		{
			Address address = new Address();
			address.setLine1(i + " PALMERSTON ROAD");
			address.setLine2("PETERBOROUGH");
			address.setPostcode("PE2 9DG");
			
			addressesList.add(address);
		}

		return addressesList;
	}
}

