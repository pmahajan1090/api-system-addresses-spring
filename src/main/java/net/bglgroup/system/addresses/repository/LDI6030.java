package net.bglgroup.system.addresses.repository	;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.MultiValueMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import net.bglgroup.spring.common.rpgcallprogram.errors.RPGException;
import net.bglgroup.spring.common.rpgcallprogram.requests.RPGRequest;
import net.bglgroup.spring.common.rpgframework.configuration.ConnectionDetailsConfiguration;
import net.bglgroup.spring.common.rpgframework.programcall.RPGProgram;
import net.bglgroup.spring.common.apiframework.model.Action;
import net.bglgroup.spring.common.apiframework.requests.ApiRequest;
import net.bglgroup.spring.common.apientry.headers.Headers;
import net.bglgroup.system.addresses.model.Address;

@Component
public class LDI6030
{	
	private final String programName = "LDI6030";
					
	@Autowired
	private List<Action> updateAddressActions;
	
	@Autowired
	private Headers apiHeaders;
	
	@Autowired
	private RPGProgram rpgProgram;
	
	@Autowired
	private ConnectionDetailsConfiguration connectionDetailsProperties;
	
	
	private RPGRequest createRPGRequest(String addressId, ApiRequest request)
	{		
		return new RPGRequest(connectionDetailsProperties.getReadOnlyConnectionDetails(request.isBasketContext()), 
				              programName, apiHeaders.transformRequestHeaders(request.getHeadersMap()), transformRequest(addressId, request));
    }
	
	public Address callProgram(String addressId, ApiRequest request) throws RPGException
	{   				
		RPGRequest rpgRequest = createRPGRequest(addressId,  request);
		
		//rpgProgram.callProgram(rpgRequest);	
		
		return tranformResponse(request);
	}
		
	private Map<String, Object> transformRequest(String addressId, ApiRequest request)
	{				
		Map<String, Object> inputBodyHashMap = Map.of("INCLUDEALLOWABLEACTIONS", request.actionsRequested() ? "Y" : "N",
									                  "ADDRESSID", addressId);
		
		return inputBodyHashMap;
	}
	
	private Address tranformResponse(ApiRequest request) throws RPGException
	{
		Address address = new Address();
				
		address.setId("283473233");
		address.setLine1("2 PALMERSTON ROAD");
		address.setLine2("PETERBOROUGH");
		address.setPostcode("PE2 9DG");
		
		return address;
	}
}
