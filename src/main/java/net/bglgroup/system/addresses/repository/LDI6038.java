package net.bglgroup.system.addresses.repository;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import net.bglgroup.spring.common.rpgcallprogram.errors.RPGException;
import net.bglgroup.spring.common.rpgcallprogram.requests.RPGRequest;
import net.bglgroup.spring.common.rpgframework.configuration.ConnectionDetailsConfiguration;
import net.bglgroup.spring.common.rpgframework.programcall.RPGProgram;
import net.bglgroup.spring.common.apiframework.requests.ApiRequest;
import net.bglgroup.spring.common.apientry.headers.Headers;

@Component
public class LDI6038
{	
	private final String programName = "LDI6038";
	
	@Autowired
	private Headers apiHeaders;
	
	@Autowired
	private RPGProgram rpgProgram;
				
	@Autowired
	private ConnectionDetailsConfiguration connectionDetailsProperties;

	private RPGRequest createRPGRequest(String addressId, ApiRequest request)
	{		
		return new RPGRequest(connectionDetailsProperties.getInsertUpdateDeleteConnectionDetails(request.isBasketContext()), 
				              programName, apiHeaders.transformRequestHeaders(request.getHeadersMap()), transformRequest(addressId, request));
    }	
	
	public void callProgram(String addressId, ApiRequest request) throws RPGException
	{   		
        RPGRequest rpgRequest = createRPGRequest(addressId,  request);
		//rpgProgram.callProgram(rpgRequest);		
	}
	
	private Map<String, Object> transformRequest(String addressId, ApiRequest request)
	{				
		JSONObject json = (JSONObject) request.getPayload();
		
		var inputBodyHashMap = new HashMap<String,Object>();
		
		inputBodyHashMap.put("UPDATEADDRESS.ID", addressId);		
    	inputBodyHashMap.put("UPDATEADDRESS.LINE1", json.query("/line1"));
		inputBodyHashMap.put("UPDATEADDRESS.LINE2", json.query("/line2"));
		inputBodyHashMap.put("UPDATEADDRESS.LINE3", json.query("/line3"));
		inputBodyHashMap.put("UPDATEADDRESS.LINE4", json.query("/line4"));
		inputBodyHashMap.put("UPDATEADDRESS.LINE5", json.query("/line5"));
		inputBodyHashMap.put("UPDATEADDRESS.POSTCODE", json.query("/postcode"));
		
		
		return inputBodyHashMap;
	}
}
