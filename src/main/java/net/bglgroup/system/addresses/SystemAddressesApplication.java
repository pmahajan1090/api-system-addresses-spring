package net.bglgroup.system.addresses;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"net.bglgroup.system.addresses", 
		                       "net.bglgroup.spring.common.rpgframework"})
public class SystemAddressesApplication 
{
    public static void main(String[] args) 
    {
        SpringApplication.run(SystemAddressesApplication.class, args);
    }
}
