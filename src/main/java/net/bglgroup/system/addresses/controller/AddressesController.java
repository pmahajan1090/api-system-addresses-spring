package net.bglgroup.system.addresses.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import net.bglgroup.spring.common.apientry.enums.HeadersEnum;
import net.bglgroup.spring.common.apiframework.exceptions.BadRequestException;
import net.bglgroup.spring.common.apiframework.requests.ApiJsonRequest;
import net.bglgroup.spring.common.rpgcallprogram.errors.RPGException;
import net.bglgroup.system.addresses.model.Address;
import net.bglgroup.system.addresses.service.AddressesService;

@RestController
public class AddressesController 
{	
	@Autowired
	private  HeadersEnum[] apiHeadersEnum;
	
    @Autowired
    private AddressesService addressesService;
                              
        
    @GetMapping(value = "/addresses", produces = "application/json")
    public ResponseEntity<ArrayList<Address>> getAddressByPostcode(HttpServletRequest request) throws RPGException, BadRequestException 
    {  		
    	return ResponseEntity.ok().body(addressesService.getAddressByPostcode(new ApiJsonRequest(request, apiHeadersEnum))); 
    }
    
    @GetMapping(value = "/addresses/{addressId}", produces = "application/json")
    public ResponseEntity<Address> getAddressById(@PathVariable("addressId") String addressId, HttpServletRequest request) throws RPGException, BadRequestException
    {      	    	    	 	    	 
    	return ResponseEntity.ok().body(addressesService.getAddressById(addressId, new ApiJsonRequest(request, apiHeadersEnum)));
    }
    
    @PatchMapping(value = "/addresses/{addressId}", consumes="application/json")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> patchAddressById(@PathVariable("addressId") String addressId, HttpServletRequest request) throws RPGException, BadRequestException
    {      	    	    	 	
    	addressesService.patchAddressById(addressId, new ApiJsonRequest(request, apiHeadersEnum));
       
    	return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
     
}
