package net.bglgroup.system.addresses.configuration;

import java.util.ArrayList;
import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.atlassian.oai.validator.OpenApiInteractionValidator;
import com.atlassian.oai.validator.springmvc.OpenApiValidationInterceptor;

import net.bglgroup.spring.common.apiframework.model.Action;
import net.bglgroup.spring.common.rpgframework.enums.RpgHeaderMappingEnum;
import net.bglgroup.spring.common.apientry.enums.ApiRequestEnum;
import net.bglgroup.spring.common.apientry.enums.HeadersEnum;
import net.bglgroup.spring.common.apientry.headers.ApiHeaders;
import net.bglgroup.spring.common.apientry.headers.Headers;
import net.bglgroup.spring.common.apientry.loggers.HttpLogger;
import net.bglgroup.spring.common.apientry.loggers.Logger;

@Configuration
public class SpringConfiguration 
{    	
	 
	 @Bean 
     public Headers apiHeaders()
	 {
 		return new ApiHeaders(RpgHeaderMappingEnum.values());
  	 }
	 
	 @Bean 
     public HeadersEnum[] apiHeadersEnum()
	 {
 		return RpgHeaderMappingEnum.values();
  	 }
	 
	 @Bean
	 public OpenApiValidationInterceptor openApiValidationInterceptor()
	 {
		 // The minimum and maximum sizes dont seem to be validated in swagger.
	     return new OpenApiValidationInterceptor(OpenApiInteractionValidator.createFor("openApi/system-addresses-1.0.0.yaml").build());
	 }	 
	 
	 @Bean
     public Logger<HttpServletRequest, HttpServletResponse> apiLogger() 
     {
     	 return new HttpLogger<HttpServletRequest, HttpServletResponse>(ApiRequestEnum.SYSTEM.name());
     }
	 
	 @Bean
	 public ArrayList<Action> updateAddressActions() 
	 {
		 ArrayList<String> fields = new ArrayList<String>( Arrays.asList("line1","line2","line3","line4","line5","postcode"));
	     Action action = new Action("update-address", "Update the Address resource using PATCH", "PATCH", fields);	
	     return new ArrayList<Action>(Arrays.asList(action));
	 }
}