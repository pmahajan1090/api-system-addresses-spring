package net.bglgroup.system.addresses.contractvalidation;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import net.bglgroup.spring.common.rpgcallprogram.builders.RPGRequestBuilder;
import net.bglgroup.spring.common.rpgcallprogram.requests.RPGRequest;
import net.bglgroup.spring.common.rpgframework.programcall.RPGProgram;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class PcmlTests 
{
    @Autowired
	private RPGProgram  rpgProgram;
	
    @Test
	public void ldi6030PcmlTest()
    {						
		assertDoesNotThrow(() ->
		{	    	
			RPGRequest rpgRequest = new RPGRequestBuilder().setProgramName("LDI6030")
					.addHeader("XTRACEID", "trace-id")
	                .addHeader("XREQUESTID", "request-id")
	                .addHeader("XMESSAGEID", "message-id")
	                .addHeader("AMENDMENTTRANSACTIONID", "1")
	                .addHeader("CUSTOMERACCOUNTID", "123456789")
	                .addBody("INCLUDEALLOWABLEACTIONS", "N")
	                .addBody("ADDRESSID", "123456789-R").build();	
						
			rpgProgram.createPCMLDocument(rpgRequest);
			rpgProgram.loadPCMLDocument(rpgRequest);
	    });
	}
    
    @Test
   	public void tbi6020PcmlTest()
       {						
   		assertDoesNotThrow(() ->
   		{
   			RPGRequest rpgRequest = new RPGRequestBuilder().setProgramName("TBI6020")
					.addHeader("XTRACEID", "trace-id")
	                .addHeader("XREQUESTID", "request-id")
	                .addHeader("XMESSAGEID", "message-id")
	                .addHeader("AMENDMENTTRANSACTIONID", "1")
	                .addHeader("CUSTOMERACCOUNTID", "123456789")
	                .addBody("POSTCODE", "PE11 5HY")
	                .addBody("PROPERTYNUMBERORNAME", "").build();	
   			
   			rpgProgram.createPCMLDocument(rpgRequest);
   			rpgProgram.loadPCMLDocument(rpgRequest);
   	    });
   	}
    
	@Test
	public void ldi6038PcmlTest()
    {						
		assertDoesNotThrow(() ->
		{
			RPGRequest rpgRequest = new RPGRequestBuilder().setProgramName("LDI6038")
					.addHeader("XTRACEID", "trace-id")
	                .addHeader("XREQUESTID", "request-id")
	                .addHeader("XMESSAGEID", "message-id")
	                .addHeader("AMENDMENTTRANSACTIONID", "1")
	                .addHeader("CUSTOMERACCOUNTID", "123456789")
	                .addBody("UPDATEADDRESS.ID", "123456789-R")
	                .addBody("UPDATEADDRESS.LINE1", "line1")
	                .addBody("UPDATEADDRESS.LINE2", "line2")
	                .addBody("UPDATEADDRESS.LINE3", "line3")
	                .addBody("UPDATEADDRESS.LINE4", "line4")
	                .addBody("UPDATEADDRESS.LINE5", "line5")
	                .addBody("UPDATEADDRESS.POSTCODE", "postcode").build();	
			
			rpgProgram.createPCMLDocument(rpgRequest);
			rpgProgram.loadPCMLDocument(rpgRequest);
	    });
	}
}
