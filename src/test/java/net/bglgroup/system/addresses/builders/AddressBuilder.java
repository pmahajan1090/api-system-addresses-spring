package net.bglgroup.system.addresses.builders;

import java.util.List;

import net.bglgroup.spring.common.apiframework.model.Action;
import net.bglgroup.system.addresses.model.Address;

public class AddressBuilder 
{     
    private String id;
	private String line1;
    private String line2;
    private String line3;
    private String line4;
    private String line5;
    private String postCode;  
    private List<Action> actions;

    public AddressBuilder setActions(List<Action> actions) {
		this.actions = actions;
		
		return this;
	}
    
	public AddressBuilder setId(String id) {
		this.id = id;
		
		return this;
	}

	public AddressBuilder setLine1(String line1) {
		this.line1 = line1;
		
		return this;
	}

	public AddressBuilder setLine2(String line2) {
		this.line2 = line2;
		
		return this;
	}

	public AddressBuilder setLine3(String line3) {
		this.line3 = line3;
		
		return this;
	}

	public AddressBuilder setLine4(String line4) {
		this.line4 = line4;
		
		return this;
	}

	public AddressBuilder setLine5(String line5) {
		this.line5 = line5;
		
		return this;
	}

	public AddressBuilder setPostCode(String postCode) {
		this.postCode = postCode;
		
		return this;
	}
	
    public Address build()
    {
    	return new Address(id, line1, line2, line3, line4, line5, postCode, actions);
    }
}
