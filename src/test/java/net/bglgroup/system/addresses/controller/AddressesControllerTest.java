package net.bglgroup.system.addresses.controller;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import net.bglgroup.spring.common.rpgframework.enums.RpgHeaderMappingEnum;
import net.bglgroup.system.addresses.service.AddressesService;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class AddressesControllerTest 
{
	private MockMvc mvc;
			
	@MockBean
    private AddressesService addressesService;
	
	@Autowired
	private AddressesController addressesController;
	
	@BeforeEach
	public void setup() 
	{
		mvc = MockMvcBuilders.standaloneSetup(addressesController).build();
	}

	@Test
	public void getAddressByIdTest()
	{    	
		assertDoesNotThrow(() ->
		{
			mvc.perform( MockMvcRequestBuilders
				      .get("/addresses/{addressId}", "123456789-R")
				      .accept(MediaType.APPLICATION_JSON)
				      .header(RpgHeaderMappingEnum.AUTHORIZATION.getName(), "123456789")
				      .header(RpgHeaderMappingEnum.MESSAGE_ID.getName(), "XMessageId")
				      .header(RpgHeaderMappingEnum.REQUEST_ID.getName(), "XRequestId")
				      .header(RpgHeaderMappingEnum.TRACE_ID.getName(), "XTraceId"))
				      .andDo(print())
				      .andExpect(status().isOk());
				      //.andExpect(content().json("{}"));
		});
	}
	
	@Test
	public void getAddressByPostcodeTest()
	{     
		assertDoesNotThrow(() ->
		{
			mvc.perform( MockMvcRequestBuilders
				      .get("/addresses?postcode={postcode}", "PE11 6AS")
				      .accept(MediaType.APPLICATION_JSON)
				      .header(RpgHeaderMappingEnum.AUTHORIZATION.getName(), "123456789")
				      .header(RpgHeaderMappingEnum.MESSAGE_ID.getName(), "XMessageId")
				      .header(RpgHeaderMappingEnum.REQUEST_ID.getName(), "XRequestId")
				      .header(RpgHeaderMappingEnum.TRACE_ID.getName(), "XTraceId"))
				      .andDo(print())
				      .andExpect(status().isOk());
		});
	}
	
	@Test
	public void patchAddressTest() 
	{  		
		assertDoesNotThrow(() ->
		{
			mvc.perform( MockMvcRequestBuilders
				      .patch("/addresses/{addressId}", "123456789-R")
				      .accept(MediaType.APPLICATION_JSON)
				      .header(RpgHeaderMappingEnum.AUTHORIZATION.getName(), "123456789")
				      .header(RpgHeaderMappingEnum.MESSAGE_ID.getName(), "XMessageId")
				      .header(RpgHeaderMappingEnum.REQUEST_ID.getName(), "XRequestId")
				      .header(RpgHeaderMappingEnum.TRACE_ID.getName(), "XTraceId")
				      .contentType(MediaType.APPLICATION_JSON)
				      .content("{ line1:line1, line2:line2, line3:line3, line4:line4, line5:line5, postcode:postcode }"))
				      .andDo(print())
				      .andExpect(status().isNoContent());
		});
	}

}
