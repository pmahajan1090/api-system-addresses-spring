package net.bglgroup.system.addresses.repository;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import net.bglgroup.spring.common.apiframework.exceptions.BadRequestException;
import net.bglgroup.spring.common.apiframework.requests.ApiRequest;
import net.bglgroup.spring.common.apientry.headers.Headers;
import net.bglgroup.spring.common.rpgcallprogram.errors.RPGException;
import net.bglgroup.spring.common.rpgframework.programcall.RPGProgram;
import net.bglgroup.system.addresses.builders.AddressBuilder;
import net.bglgroup.system.addresses.model.Address;
import net.bglgroup.system.addresses.repository.TBI6020;

@ExtendWith(SpringExtension.class)
@SpringBootTest
//@ActiveProfiles("development")
public class Tbi6020Tests 
{	    
    @MockBean
	private Headers apiHeaders;
	@MockBean
	private RPGProgram        rpgProgram;
		
	@Autowired 
	private TBI6020 tbi6020;
	

	@Mock
	private ApiRequest apiRequest;	
	
	private Address expectedAddress;
	
    @BeforeEach
	public void initObjects() throws RPGException, BadRequestException, IOException 
	{						
		Mockito.when(apiHeaders.transformRequestHeaders(apiRequest.getHeadersMap())).thenReturn(new HashMap<String, Object>());
		Mockito.when(apiRequest.getQueryParemeter("postcode")).thenReturn("AB1 1AB");
				
    	expectedAddress = new AddressBuilder().setLine1("line1")
    										  .setLine1("line2")
    										  .setLine1("line3")
    										  .setLine1("line4")
    										  .setLine1("line5")
    										  .setPostCode("AB1 1AB").build();
    	
    	Mockito.when(rpgProgram.getResponseValue("ADDRESSCOUNT")).thenReturn(1);
    	
    	Mockito.when(rpgProgram.getArrayResponseValue("ADDRESS.LINE1", 0)).thenReturn(expectedAddress.getLine1());
    	Mockito.when(rpgProgram.getArrayResponseValue("ADDRESS.LINE2", 0)).thenReturn(expectedAddress.getLine2());
    	Mockito.when(rpgProgram.getArrayResponseValue("ADDRESS.LINE3", 0)).thenReturn(expectedAddress.getLine3());
    	Mockito.when(rpgProgram.getArrayResponseValue("ADDRESS.LINE4", 0)).thenReturn(expectedAddress.getLine4());
    	Mockito.when(rpgProgram.getArrayResponseValue("ADDRESS.LINE5", 0)).thenReturn(expectedAddress.getLine5());
    	Mockito.when(rpgProgram.getArrayResponseValue("ADDRESS.POSTCODE", 0)).thenReturn(expectedAddress.getPostcode());
    }
	
	@Test
	public void getAddressByPostcodeTest()
    {						
		assertDoesNotThrow(() ->
		{
			ArrayList<Address> actualAddresses = tbi6020.callProgram(apiRequest);
			
			for (Address actualAddress : actualAddresses)
			{
			    assertEquals(expectedAddress.toString(), actualAddress.toString());
			}
		});
	}
}
