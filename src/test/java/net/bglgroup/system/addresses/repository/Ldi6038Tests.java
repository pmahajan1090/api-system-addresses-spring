package net.bglgroup.system.addresses.repository;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import net.bglgroup.spring.common.apiframework.exceptions.BadRequestException;
import net.bglgroup.spring.common.apiframework.requests.ApiJsonRequest;
import net.bglgroup.spring.common.apiframework.requests.ApiRequest;
import net.bglgroup.spring.common.apientry.enums.HeadersEnum;
import net.bglgroup.spring.common.apientry.headers.Headers;
import net.bglgroup.spring.common.rpgcallprogram.errors.RPGException;
import net.bglgroup.spring.common.rpgframework.programcall.RPGProgram;
import net.bglgroup.system.addresses.repository.LDI6038;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class Ldi6038Tests 
{
	private final String addressId = "123456789-R";	
	
	@MockBean
	private Headers apiHeaders;
	@MockBean
	private RPGProgram        rpgProgram;
		
	@Autowired
	private LDI6038 ldi6038;
	
	@Autowired
	private HeadersEnum[] apiHeadersEnum;
	
	private HttpServletRequest  request;
	
	@BeforeEach
	public void initObjects() throws RPGException, BadRequestException, IOException 
	{				
		request = Mockito.mock(HttpServletRequest.class);
		
		Mockito.when(request.getMethod()).thenReturn(HttpMethod.PATCH.name());
		
		List<String> headerlist = new ArrayList<String>();
		for (HeadersEnum header : apiHeadersEnum)
		{
			headerlist.add(header.getName());
			Mockito.when(request.getHeader(header.getName())).thenReturn(header.getName());
		}
		Mockito.when(request.getHeaderNames()).thenReturn(Collections.enumeration(headerlist));	
		Mockito.when(request.getReader()).thenReturn(new BufferedReader(new StringReader("")));			
	 }
	
	@Test
	public void patchAddressByIdTest()
    {				
		assertDoesNotThrow(() ->
		{
			Mockito.when(request.getReader()).thenReturn(new BufferedReader(new StringReader("{ line1:line1, line2:line2, line3:line3, line4:line4, line5:line5, postcode:postcode }")));
	
	        ApiRequest apiRequest = new ApiJsonRequest(request, apiHeadersEnum);
		    Mockito.when(apiHeaders.transformRequestHeaders(apiRequest.getHeadersMap())).thenReturn(new HashMap<String, Object>());
	    
			ldi6038.callProgram(addressId, apiRequest);
		});
	}
	
	@Test
	public void patchAddressByIdWithNullsTest()
    {			
		assertDoesNotThrow(() ->
		{    
			Mockito.when(request.getReader()).thenReturn(new BufferedReader(new StringReader("{ dummyfield:value }")));
				
			ApiRequest apiRequest = new ApiJsonRequest(request, apiHeadersEnum);
		    Mockito.when(apiHeaders.transformRequestHeaders(apiRequest.getHeadersMap())).thenReturn(new HashMap<String, Object>());
	    
			ldi6038.callProgram(addressId, apiRequest);
		});
	}
}
