package net.bglgroup.system.addresses.repository;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import java.util.List;

import org.apache.commons.collections.map.MultiValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import net.bglgroup.spring.common.apiframework.model.Action;
import net.bglgroup.spring.common.apiframework.requests.ApiRequest;
import net.bglgroup.spring.common.apientry.headers.Headers;
import net.bglgroup.spring.common.rpgcallprogram.errors.RPGException;
import net.bglgroup.spring.common.rpgframework.programcall.RPGProgram;
import net.bglgroup.system.addresses.builders.AddressBuilder;
import net.bglgroup.system.addresses.model.Address;
import net.bglgroup.system.addresses.repository.LDI6030;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class Ldi6030Tests 
{
	private final String addressId = "123456789-R";	

	@MockBean
	private Headers apiHeaders;
	@MockBean
	private RPGProgram        rpgProgram;
		
	@Autowired 
	private LDI6030 ldi6030;
	
	@Autowired
	private List<Action> updateAddressActions;
	
	@Mock
	private ApiRequest apiRequest;	
	
	private Address expectedAddress;
	
	@BeforeEach
	public void initObjects() throws RPGException
	{								
		MultiValueMap actions = new MultiValueMap();
		actions.put("123456789", "changeAddressYN");
		
		
		// Create the address expected that we can assert against
		expectedAddress = new AddressBuilder().setId("123456789-R")
											  .setLine1("line1")
											  .setLine1("line2")
											  .setLine1("line3")
											  .setLine1("line4")
											  .setLine1("line5")
											  .setPostCode("AB1 1AB")
											  .setActions(updateAddressActions).build();
    	
		

		// Create RPG Request Mocks dont need to have actual headers as rpgProgram is mocked
		Mockito.when(apiHeaders.transformRequestHeaders(apiRequest.getHeadersMap())).thenReturn(new HashMap<String, Object>());
		Mockito.when(apiRequest.actionsRequested()).thenReturn(true);
		
		// Transform response mocks
		Mockito.when(rpgProgram.getResponseValue("ADDRESS.CUSTOMERACCOUNTID")).thenReturn("123456789");
    	Mockito.when(rpgProgram.getResponseValue("ADDRESS.ID")).thenReturn(expectedAddress.getId());	
		Mockito.when(rpgProgram.getResponseValue("ADDRESS.LINE1")).thenReturn(expectedAddress.getLine1());			
		Mockito.when(rpgProgram.getResponseValue("ADDRESS.LINE2")).thenReturn(expectedAddress.getLine2());	
		Mockito.when(rpgProgram.getResponseValue("ADDRESS.LINE3")).thenReturn(expectedAddress.getLine3());	
		Mockito.when(rpgProgram.getResponseValue("ADDRESS.LINE4")).thenReturn(expectedAddress.getLine4());	
		Mockito.when(rpgProgram.getResponseValue("ADDRESS.LINE5")).thenReturn(expectedAddress.getLine5());	
		Mockito.when(rpgProgram.getResponseValue("ADDRESS.POSTCODE")).thenReturn(expectedAddress.getPostcode());						
		Mockito.when(rpgProgram.getAllowableActions("CUSTOMERACCOUNTID")).thenReturn(actions);
    }
	
	@Test
	public void getAddressByIdTest()
    {		
		assertDoesNotThrow(() ->
		{
			Address actualAddress = ldi6030.callProgram(addressId, apiRequest);		
			
			assertEquals(expectedAddress.toString(), actualAddress.toString());	
		});
	}
}
