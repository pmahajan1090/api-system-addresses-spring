package net.bglgroup.system.addresses.service;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import net.bglgroup.spring.common.apiframework.requests.ApiRequest;
import net.bglgroup.system.addresses.builders.AddressBuilder;
import net.bglgroup.system.addresses.model.Address;
import net.bglgroup.system.addresses.repository.LDI6030;
import net.bglgroup.system.addresses.repository.LDI6038;
import net.bglgroup.system.addresses.repository.TBI6020;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class AddressesServiceTest 
{

	private final String addressId = "123456789-R";	

	@MockBean
	private LDI6030 ldi6030;
	@MockBean
	private LDI6038 ldi6038;
	@MockBean
	private TBI6020 tbi6020;
	
	@Mock
	private ApiRequest apiRequest;
	
	@Autowired
	private AddressesService addressesService;
	
	private Address expectedAddress;
	
	@BeforeEach
	public void initObjects()
	{	
		expectedAddress = new AddressBuilder().setId(addressId)
											  .setLine1("line1")
											  .setLine1("line2")
											  .setLine1("line3")
											  .setLine1("line4")
											  .setLine1("line5")
											  .setPostCode("AB1 1AB").build();
	}
	
	@Test
	public void getAddressByIdTest()
    {
		assertDoesNotThrow(() ->
		{			
			Mockito.when(ldi6030.callProgram(addressId,apiRequest)).thenReturn(expectedAddress);
			
			assertEquals(expectedAddress.toString(), addressesService.getAddressById(addressId, apiRequest).toString());
		});

	}
	
	@Test
	public void getAddressByPostcodeTest()
    {			
		assertDoesNotThrow(() ->
		{
			ArrayList<Address> expectedAddressList = new ArrayList<Address>(Arrays.asList(expectedAddress));				
			
			Mockito.when(tbi6020.callProgram(apiRequest)).thenReturn(expectedAddressList);
			
			ArrayList<Address> actualAddressList = addressesService.getAddressByPostcode(apiRequest);
			
			for (int i=0; i < expectedAddressList.size(); i++)  
			{
			    assertEquals(expectedAddressList.get(i).toString(), actualAddressList.get(i).toString());
			}
		});
	}
	
	@Test
	public void patchAddressById()
    {		
		assertDoesNotThrow(() ->
		{
			addressesService.patchAddressById(addressId, apiRequest);
		});
	}

}
