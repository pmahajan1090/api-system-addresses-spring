

# api-system-addresses-spring

This is spring boot based example application for system layer api.
This uses program-call library to communicate to the DISC rpg programs
This is integrated with the swagger 2 which generated the dynamic API documenttaion to view and try the deployed API

## Usage

Import project / pom file in editor like intelij / eclipse
Start net.bglgroup.system.addresses.SystemAddressesApplication java program in the editor

## Start with maven
This project can be started from maven using commnd

```console
mvn spring-boot:run
```


## Link for swagger documentation
http://localhost:8080/swagger-ui.html